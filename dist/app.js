"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var cron = __importStar(require("node-cron"));
var promise = __importStar(require("./methods/api"));
cron.schedule("* * * * * *", function () {
    console.log("converting every minute");
    promise.api();
});

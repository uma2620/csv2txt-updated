"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var csvtojson_1 = __importDefault(require("csvtojson"));
var converter = __importStar(require("./jsontotxt"));
var csvFilePath = './files/sample.csv';
exports.toJson = function (data) {
    var jsonData;
    csvtojson_1.default().fromFile(csvFilePath)
        .then(function (result) {
            
        result=result.concat(data);
        jsonData=result;
        converter.toTxt(jsonData);
    });
};

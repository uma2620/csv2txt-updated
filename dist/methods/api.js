"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var request = __importStar(require("request"));
var converter = __importStar(require("./csvtojson"));
var userDetails;
function getData() {
    // Setting URL and headers for request
    var options = {
        url: "https://api.github.com/users",
        headers: {
            'User-Agent': 'request'
        }
    };
    // Return new promise 
    return new Promise(function (resolve, reject) {
        // Do async job
        request.get(options, function (err, resp, body) {
            if (err) {
                reject(err);
            }
            else {
                resolve(JSON.parse(body));
            }
        });
    });
}
var errHandler = function (err) {
    console.log(err);
};
exports.api = function () {
    var dataPromise = getData();
    // Get user details after that get followers from URL
    dataPromise.then(function (result) {
        userDetails = result;
        console.log("user details fetched");
        converter.toJson(userDetails);
        // Do one more async operation here
    }, errHandler);
};

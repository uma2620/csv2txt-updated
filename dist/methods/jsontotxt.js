"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = __importStar(require("fs"));
var txtFilePath = './files/output.txt';
exports.toTxt = function (result) {
    var jsonData;
    jsonData = JSON.stringify(result, null, 1);
    //console.log("hi");
    //console.log(data);
    fs.writeFile(txtFilePath, jsonData, function (result, err) {
        if (err) {
            console.log(err);
        }
        else {
            console.log("succesfully converted");
        }
    });
};

import * as request from 'request';
import * as converter from './csvtojson';
 var userDetails:any;
function getData() {
    // Setting URL and headers for request
    var options = {
        url: "https://api.github.com/users",
        headers: {
            'User-Agent': 'request'
        }
    };
    // Return new promise 
    return new Promise(function(resolve, reject) {
        // Do async job
        request.get(options, function(err:any, resp:any, body:any) {
            if (err) {
                reject(err);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })
}

var errHandler = function(err: any) {
    console.log(err);
}

export const api:any=()=> {
    var dataPromise = getData();
    // Get user details after that get followers from URL
    dataPromise.then(function(result: any) {
                    userDetails = result;
                    console.log("user details fetched")
                    converter.toJson(userDetails)
                    // Do one more async operation here
                }, errHandler)
                
}



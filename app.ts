import * as cron from 'node-cron';
import * as promise from './methods/api';

cron.schedule("* * * * * *", function() {
    console.log("converting every minute");
    promise.api();
  });